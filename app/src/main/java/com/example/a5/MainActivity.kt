package com.example.a5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var buttonSubmit: Button
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRepeatPassword: EditText
    private var countAddressSign = 0
    private var countDigit = 0
    private var countLetter = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        errorsCheck()
    }
        private fun init() {
            buttonSubmit = findViewById(R.id.buttonSubmit)
            editTextEmail = findViewById(R.id.editTextEmail)
            editTextPassword = findViewById(R.id.editTextPassword)
            editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        }
    private fun errorsCheck() {
        buttonSubmit.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password1 = editTextPassword.text.toString()
            val password2 = editTextRepeatPassword.text.toString()
            for (i in email.indices)
                if (email[i].toString() == "@" ){
                    countAddressSign++ }
            for (i in password1.indices)
                if (password1[i].isDigit()){
                    countDigit ++ }
            for (i in password1.indices)
                if (password1[i].isLetter()){
                    countLetter++ }

            if (email.contains('@') && countLetter > 1 && countDigit >= 1 && password1.length >= 9 && password1 == password2) {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password1)
                Toast.makeText(this, "რეგისტრაცია წარმატებით დასრულდა!", Toast.LENGTH_SHORT).show() }


            if (countAddressSign != 1) {
                editTextEmail.error = "ელექტრონული ფოსტის მისამართი უნდა შეიცავდეს ერთ @ !" }
            if (!email.contains('@')) {
                editTextEmail.error = "შეიყვანეთ იმეილი სწორად!"
                return@setOnClickListener }
            if (countLetter < 1) {
                editTextPassword.error = "შეიყვანეთ ერთი სიმბოლო მაინც!"
                return@setOnClickListener }
            if (countDigit < 1) {
                editTextPassword.error = "პაროლი უნდა შეიცავდეს 1 ციფრს მაინც!"
                return@setOnClickListener }
            if (password1.length < 9) {
                editTextPassword.error = "პაროლი უნდა იყოს მინიმუმ 9 სიმბოლო!"
                return@setOnClickListener }
            if (password1 != password2) {
                editTextRepeatPassword.error = "პაროლები არ ემთხვევა!"
                editTextPassword.error = "პაროლები არ ემთხვევა!"
                return@setOnClickListener }
        }
    }
    }

